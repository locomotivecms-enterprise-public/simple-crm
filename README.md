# SimpleCRM

This is a very minimal CRM to show how to use the Locomotive SiteBuilder API to create sites from CRM data.

## Requirements

You need the email and the API key we sent to you.

```shell
export SITE_BUILDER_PLATFORM_EMAIL=<your email>
export SITE_BUILDER_PLATFORM_API_KEY=<your API KEY>
```

## Installation

```shell
git clone git@gitlab.com:locomotivecms-enterprise-public/simple-crm.git
cd simple-crm
bundle
rake db:create
rake db:migrate
```

## Launch

```shell
foreman start
```

Open your browser at the following address: **[http://0.0.0.0:5001/](http://0.0.0.0:5001/)**.

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

