module SitesHelper

  def select_options_for_themes
    $site_builder_client.themes.all.map do |theme|
      [theme.name, theme.id]
    end
  end

end
