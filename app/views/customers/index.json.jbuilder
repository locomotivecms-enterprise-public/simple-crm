json.array!(@customers) do |customer|
  json.extract! customer, :id, :company, :address, :phone_number, :google_plus_link, :facebook_link, :twitter_link
  json.url customer_url(customer, format: :json)
end
