class Customer < ApplicationRecord

  ## validations ##
  validates_presence_of :company, :address, :phone_number

  ## methods ##

  def has_site?
    self.site_builder_site_id.present?
  end

  def to_site_attributes
    {
      name: self.company,
      metadata: {
        address: self.address,
        phone_number: self.phone_number,
        opening_hours: self.opening_hours,
        social_links: {
          google: self.google_plus_link,
          facebook: self.facebook_link,
          twitter: self.twitter_link
        }
      }
    }
  end

end
