class Site

  include ActiveModel::Model
  include ActiveModel::Validations

  attr_accessor :theme_id, :name, :email, :password, :password_confirmation

  ## validations ##
  validates_each :theme_id, :name, :email, :password, :password_confirmation do |record, attr, value|
    record.errors.add attr, "can't be blank" if value.blank?
  end

  validates :password, confirmation: true

  def attributes
    {
      theme_id: self.theme_id,
      name: self.name,
      email: self.email,
      password: self.password
    }
  end

end
