class SitesController < ApplicationController

  before_action :set_customer, only: [:new, :create, :update]

  # GET /customers/1/site/new
  def new
    @site = Site.new(name: 'John', email: 'john@doe.net', password: 'test31', password_confirmation: 'test31')
  end

  # POST /customers/1/site
  def create
    @site = Site.new(site_params)

    respond_to do |format|
      if @site.valid?
        CreateSiteJob.perform_later(@customer.id, @site.attributes)

        format.html { redirect_to @customer, notice: 'The site is being created. Please refresh that page.' }
        format.json { render :show, status: :created, location: @customer }
      else
        format.html { render :new }
        format.json { render json: @site.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /customers/1/site
  def update
    puts "TODO CREATE UPDATE"
    redirect_to customer_path(@customer)
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_customer
      @customer = Customer.find(params[:customer_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def site_params
      params.require(:site).permit(:name, :email, :password, :password_confirmation, :theme_id)
    end

end
