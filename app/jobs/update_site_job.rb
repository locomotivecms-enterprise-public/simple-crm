class UpdateSiteJob < ApplicationJob

  queue_as :simple_crm

  def perform(customer_id)
    puts '--- UPDATE SITE JOB ---'
    puts "customer = #{customer_id}"

    customer = Customer.find(customer_id)

    api_client.sites.update(customer.site_builder_site_id, customer.to_site_attributes)
    puts "site updated! #{site.id}"
  end
end
