class ApplicationJob < ActiveJob::Base

  private

  def api_client
    $site_builder_client
  end

end
