class CreateSiteJob < ApplicationJob

  queue_as :simple_crm

  def perform(customer_id, site_attributes)
    puts '--- CREATE SITE JOB ---'
    puts "customer = #{customer_id} / #{site_attributes}"

    customer = Customer.find(customer_id)

    # create the account
    account = create_account(site_attributes.slice(:name, :email, :password, :password_confirmation))
    customer.site_builder_account_id = account.id

    # create the site
    site = create_site(account.id, site_attributes[:theme_id], customer.to_site_attributes)
    customer.site_builder_site_id = site.id

    customer.save
  end

  private

  def create_account(attributes)
    api_client.accounts.create({
      role: 'site_owner'
    }.merge(attributes)).tap do |account|
      puts "account created! #{account.id}"
    end
  end

  def create_site(account_id, theme_id, attributes)
    api_client.sites.create({
      owner_id: account_id,
      theme_id: theme_id
    }.merge(attributes)).tap do |site|
      puts "site created! #{site.id}"
    end
  end

end
