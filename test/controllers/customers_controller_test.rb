require 'test_helper'

class CustomersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @customer = customers(:one)
  end

  test "should get index" do
    get customers_url
    assert_response :success
  end

  test "should get new" do
    get new_customer_url
    assert_response :success
  end

  test "should create customer" do
    assert_difference('Customer.count') do
      post customers_url, params: { customer: { address: @customer.address, company: @customer.company, facebook_link: @customer.facebook_link, first_name: @customer.first_name, google_plus_link: @customer.google_plus_link, last_name: @customer.last_name, phone_number: @customer.phone_number, twitter_link: @customer.twitter_link } }
    end

    assert_redirected_to customer_url(Customer.last)
  end

  test "should show customer" do
    get customer_url(@customer)
    assert_response :success
  end

  test "should get edit" do
    get edit_customer_url(@customer)
    assert_response :success
  end

  test "should update customer" do
    patch customer_url(@customer), params: { customer: { address: @customer.address, company: @customer.company, facebook_link: @customer.facebook_link, first_name: @customer.first_name, google_plus_link: @customer.google_plus_link, last_name: @customer.last_name, phone_number: @customer.phone_number, twitter_link: @customer.twitter_link } }
    assert_redirected_to customer_url(@customer)
  end

  test "should destroy customer" do
    assert_difference('Customer.count', -1) do
      delete customer_url(@customer)
    end

    assert_redirected_to customers_url
  end
end
