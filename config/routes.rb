Rails.application.routes.draw do

  root to: redirect('/customers')

  resources :customers do
    resource :site, only: [:new, :create, :update, :destroy]
  end

  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'

end
