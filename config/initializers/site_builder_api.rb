credentials = {
  uri:      ENV['SITE_BUILDER_PLATFORM_URL'] || 'https://builder.locomotivecms.com',
  email:    ENV['SITE_BUILDER_PLATFORM_EMAIL'],
  api_key:  ENV['SITE_BUILDER_PLATFORM_API_KEY']
}

$site_builder_client = SiteBuilder::Client.new(credentials)

begin
  token = $site_builder_client.token
  puts "Connected to the Locomotive SiteBuilder platform!"
rescue SiteBuilder::Client::Error => e
  puts "Wrong credentials (#{credentials.inspect}). We were unable to connect to the Locomotive SiteBuilder platform.\nMake sure the SITE_BUILDER_PLATFORM_EMAIL and SITE_BUILDER_PLATFORM_API_KEY env variables are valid."
  exit 0
end
