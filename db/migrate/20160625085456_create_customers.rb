class CreateCustomers < ActiveRecord::Migration[5.0]
  def change
    create_table :customers do |t|
      t.string :company
      t.string :address
      t.string :phone_number
      t.string :google_plus_link
      t.string :facebook_link
      t.string :twitter_link

      t.integer :site_builder_account_id
      t.integer :site_builder_site_id

      t.jsonb :opening_hours

      t.timestamps
    end
  end
end
